//
//  Movie.m
//  MoviesLab
//
//  Created by maicon douglas on 17/08/20.
//  Copyright © 2020 maicon douglas. All rights reserved.
//

#import "Movie.h"

@implementation Movie

- (id) init:(NSString *)name descv : (NSString *) desc  fv:(BOOL )favorite url:(NSString *) urlv id:(NSString *) imdbidv{
    [self setName:name];
    [self setDescription:desc];
    [self setFavorite:favorite];
    [self setURL:urlv];
    [self setIMDBID:imdbidv];
    return self;
}

- (void)setName:(NSString *)value{
    name = value;
}

- (void)setFavorite:(BOOL)value{
    favorite = value;
}

- (void)setURL:(NSString *)value{
    url = value;
}

- (void)setIMDBID:(NSString *)value{
    imdbID = value;
}

- (void)setDescription:(NSString *)value{
    description = value;
}

- (NSString *)getName{
    return name;
}

- (BOOL)getFavorite{
    return favorite;
}

- (NSString *)getURL{
    return url;
}

- (NSString *)getIMDBID{
    return imdbID;
}

- (NSString *)getDescription{
    return description;
}

@end
