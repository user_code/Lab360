//
//  Movie.h
//  MoviesLab
//
//  Created by maicon douglas on 17/08/20.
//  Copyright © 2020 maicon douglas. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface Movie : NSObject{
    NSString * name;
    NSString * description;
    BOOL favorite;
    NSString * url;
    NSString * imdbID;
}

- (id) init :(NSString *) name descv :(NSString *) desc fv: (BOOL) favorite url: (NSString *) urlv id:(NSString *) imdbidv;

- (void)setName : (NSString *)name;
- (void)setFavorite : (BOOL)favorite;
- (void)setURL : (NSString *) url;
- (void)setIMDBID : (NSString *) imdbID;
- (void)setDescription : (NSString *) description;

- (NSString *) getName;
- (BOOL) getFavorite;
- (NSString *) getURL;
- (NSString *) getIMDBID;
- (NSString *) getDescription;

@end

NS_ASSUME_NONNULL_END
