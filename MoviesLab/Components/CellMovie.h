//
//  CellMovie.h
//  MoviesLab
//
//  Created by maicon douglas on 17/08/20.
//  Copyright © 2020 maicon douglas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "../Controllers/ViewController.h"//;

NS_ASSUME_NONNULL_BEGIN

@interface CellMovie : UICollectionViewCell

@property (nonatomic,weak) IBOutlet UIImageView *image;
@property (nonatomic,strong) ViewController *controller;

@property (strong,nonatomic) UIImageView *imageView;
@property (strong,nonatomic) UILabel *title;
@property (strong,nonatomic) UIView * chip;
@property (strong,nonatomic) UILabel *description;
@property (readonly,nonatomic) UIButton *buttonStart;

- (void) setImageWithURL : (NSURL *) url;

@end

NS_ASSUME_NONNULL_END
