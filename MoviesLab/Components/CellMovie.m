//
//  CellMovie.m
//  MoviesLab
//
//  Created by maicon douglas on 17/08/20.
//  Copyright © 2020 maicon douglas. All rights reserved.
//

#import "CellMovie.h"
#import <UIKit/UIKit.h>

@interface CellMovie()

@end

@implementation CellMovie

@synthesize description;

- (instancetype)init
{
    self = [super init];
    if (self) {
        
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setupLayout];
        [self setupEvent];
    }
    return self;
}

#pragma setImageAsync

- (void)setImageWithURL:(NSURL *)url{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        NSData * data = [NSData dataWithContentsOfURL:url];
        if (data != nil) {
            UIImage * image = [UIImage imageWithData:data];
            if(image != nil){
                dispatch_async(dispatch_get_main_queue(), ^{
                   self.imageView.image = image;
                });
            }
        }
    });
}

#pragma EventLayout

- (void) setupEvent{
    [_buttonStart addTarget:self action:@selector(handleFavorite:) forControlEvents:UIControlEventTouchUpInside];
}

- (void) handleFavorite :(id) obj{
    [self.controller handlerFavorite:self];
}

#pragma SetupLayoutCell

- (void) setupLayout {
    _imageView = [[UIImageView alloc] init];
    _imageView.backgroundColor = UIColor.grayColor;
    
    _chip = [[UIView alloc] init];
    _chip.backgroundColor = UIColor.redColor;
    _chip.layer.masksToBounds = YES;
    _chip.layer.cornerRadius = 5.0f;
    
    _buttonStart = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 25, 25)];
    [_buttonStart setImage:[UIImage imageNamed:@"stari"] forState:UIControlStateNormal];
    
    _title = [[UILabel alloc] init];
    description = [[UILabel alloc] init];
    
    _title.text = @"Title Movie";
    _title.textColor = UIColor.blackColor;
    
    description.text = @"Movie for create in 1002 creative to Michael Douglas in New York";
    description.textColor = UIColor.whiteColor;
    description.font = [UIFont systemFontOfSize:14 weight:UIFontWeightBold];
    description.numberOfLines = 2;
    description.lineBreakMode = NSLineBreakByWordWrapping;
    
    [self addSubview:_imageView];
    [self addSubview:_title];
    [self addSubview:_buttonStart];
    [self addSubview:_chip];
    [_chip addSubview:description];
    
    [self setAnchor:_imageView :self.leadingAnchor :nil : nil : self.topAnchor : UIEdgeInsetsMake(10, 10, 0, 0) : CGSizeMake(65, 90)];
    [self setAnchor:_title :_imageView.trailingAnchor :nil :nil :_imageView.topAnchor : UIEdgeInsetsMake(0, 10, 0, 0) : CGSizeMake(180, 0)];
    
    [self setAnchor:_chip :_imageView.trailingAnchor :description.bottomAnchor :description.trailingAnchor :_title.bottomAnchor :UIEdgeInsetsMake(10, 10, 5, 5) :CGSizeMake(0,0)];
    [self setAnchor:description :_chip.leadingAnchor :nil :_chip.trailingAnchor :_chip.topAnchor :UIEdgeInsetsMake(5, 5, 5, 5) :CGSizeMake(0, 0)];
    
    [self setAnchor:_buttonStart :nil :nil :self.trailingAnchor :self.topAnchor :UIEdgeInsetsMake(10, 0, 0, -10) :CGSizeZero];
}

- (void) setAnchor : (nullable UIView *)view :
                     (nullable NSLayoutXAxisAnchor *) leading :
                     (nullable NSLayoutYAxisAnchor *) bottom :
                     (nullable NSLayoutXAxisAnchor *) trailing :
                     (nullable NSLayoutYAxisAnchor *) top :
                     (const UIEdgeInsets ) padding : (const CGSize) size {
    
    if (view != NULL) {
        
        [view setTranslatesAutoresizingMaskIntoConstraints:NO];
        
        if (leading != NULL) {
            [[view.leadingAnchor constraintEqualToAnchor:leading constant: padding.left ] setActive:YES];
        }
        if(bottom != NULL){
            [[view.bottomAnchor constraintEqualToAnchor:bottom constant:padding.bottom] setActive:YES];
        }
        if (trailing != NULL) {
            [[view.trailingAnchor constraintEqualToAnchor:trailing constant:padding.right] setActive:YES];
        }
        if (top != NULL) {
            [[view.topAnchor constraintEqualToAnchor:top constant:padding.top] setActive:YES];
        }
        
        if(size.width != 0){
            [[view.widthAnchor constraintEqualToConstant:size.width] setActive:YES];
        }
        if(size.height != 0){
            [[view.heightAnchor constraintEqualToConstant:size.height] setActive:YES];
        }
        
    }
}
   
@end
