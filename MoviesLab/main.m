//
//  main.m
//  MoviesLab
//
//  Created by maicon douglas on 17/08/20.
//  Copyright © 2020 maicon douglas. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
