//
//  ViewController.h
//  MoviesLab
//
//  Created by maicon douglas on 17/08/20.
//  Copyright © 2020 maicon douglas. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UICollectionViewController<UICollectionViewDataSource,UICollectionViewDelegateFlowLayout>
- (void) handlerFavorite : (UICollectionViewCell * ) cell;
- (BOOL) getFavoriteLocal : (NSString *) imdbID;
@end

