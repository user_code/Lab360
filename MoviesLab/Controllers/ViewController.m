//
//  ViewController.m
//  MoviesLab
//
//  Created by maicon douglas on 17/08/20.
//  Copyright © 2020 maicon douglas. All rights reserved.
//

#import "ViewController.h"
#import "../Components/CellMovie.h"
#import "../Models/Movie.h"//;
#import "../Api/ApiMovies.h"//;
#import <UIKit/UIKit.h>


@interface ViewController ()

@property (strong,nonatomic) IBOutlet UICollectionView *collectionContainer;
@property (strong,nonatomic) NSArray *list;
@property (strong,nonatomic) ApiMovies *api;
@property (strong,retain) NSUserDefaults * defaults;

@end

@implementation ViewController

static NSString * const reuseIndetifier = @"MovieCell";

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"Start Application");
    
    // collectionView Delegate Storyboard
    //self.collectionContainer.delegate = self;
    
    _defaults = [NSUserDefaults standardUserDefaults];
    
    [self.collectionView setBackgroundColor:UIColor.whiteColor];
    [self.collectionView setAlwaysBounceVertical:YES];
    [self.collectionView registerClass:CellMovie.class forCellWithReuseIdentifier:reuseIndetifier];
    //[self.collectionView registerClass:[UICollectionViewCell class] forCellWithReuseIdentifier:reuseIndetifier];
    [self.navigationItem setTitle:@"List Movies"];
    self.navigationController.navigationBar.prefersLargeTitles = YES;
    [self.navigationItem setLargeTitleDisplayMode:UINavigationItemLargeTitleDisplayModeAutomatic];
    
    
    _api = [[ApiMovies alloc] init];
    
    self.list = @[[[Movie alloc] init:@"Movie Title Test" descv : @"Movie Description Test" fv:YES url:@"" id:@"default1"]];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)viewWillAppear:(BOOL)animated{
    
    [_api getMovies:self :^(NSMutableArray<Movie *> * listMovie) {
        
        NSLog(@"List Start reload");
        
        dispatch_async(dispatch_get_main_queue(), ^(void){
            
            self.list = [[NSArray alloc] initWithArray:listMovie];
            [self.collectionView reloadData];
            
        });
        
    }];
}

#pragma EventViewController

- (void)handlerFavorite : (UICollectionViewCell *) cell{
    
    NSIndexPath * indexCell = [self.collectionView indexPathForCell:cell];
    Movie * cellMovie = self.list[indexCell.row];
    const BOOL hasFavorite = cellMovie.getFavorite;
    
    [self.list[indexCell.row] setFavorite:!hasFavorite];
    [self.collectionView reloadItemsAtIndexPaths:@[indexCell]];
    
    [_defaults setObject:cellMovie.getFavorite ? @"YES" : @"NO" forKey:cellMovie.getIMDBID];
    NSLog(@"local save change %@",[_defaults stringForKey:cellMovie.getIMDBID]);
}

- (BOOL) getFavoriteLocal : (NSString *) imdbID{
    return [[_defaults objectForKey:imdbID]  isEqual: @"YES"] ? YES : NO;
}

#pragma UICollectionViewDelegateFlowLayout

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    NSInteger numberItem = self.list.count;
    return numberItem;
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    CellMovie *cell = (CellMovie*) [collectionView dequeueReusableCellWithReuseIdentifier:reuseIndetifier forIndexPath:indexPath];
    cell.controller = self;
    
    Movie * movie = self.list[indexPath.row];
    
    [cell.image setImage:[UIImage imageNamed:@"play"]];
    [cell.title setText:movie.getName];
    [cell.description setText:movie.getDescription];
    [cell setImageWithURL:[NSURL URLWithString:[movie getURL]]];
    
    cell.buttonStart.imageView.image = [UIImage imageNamed:movie.getFavorite ? @"stara" : @"stari"];
    
    return cell;
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    return CGSizeMake(self.view.frame.size.width, 110);
}

@end


