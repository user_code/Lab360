//
//  ApiMovies.m
//  MoviesLab
//
//  Created by maicon douglas on 18/08/20.
//  Copyright © 2020 maicon douglas. All rights reserved.
//

#import "ApiMovies.h"
#import "../Controllers/ViewController.h"//;
#import "../Models/Movie.h"//;


@implementation ApiMovies

- (void) getMovies : (ViewController *) controller : (void(^)(NSMutableArray<Movie *> *)) completion {
    
    NSURL * urlR = [NSURL URLWithString:@urlapi];
    NSMutableURLRequest * request = [[NSMutableURLRequest alloc] init];
    [request setHTTPMethod:@"GET"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setURL:urlR];
    
    NSMutableArray<Movie *> * listMoviesMutable = [[NSMutableArray alloc] init];
    
    NSURLSessionTask *dataTask = [[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:
     ^(NSData * _Nullable data,NSURLResponse * _Nullable reponse,NSError * _Nullable error){
         if (error) {
             NSLog(@"Error Request");
         }
         
         NSDictionary *results = [NSJSONSerialization JSONObjectWithData:data options:kNilOptions error:&error];
         for (NSDictionary * movie in results[@"Search"]) {
             NSLog(@"%@",movie);
             [listMoviesMutable addObject:[[Movie alloc] init:movie[@"Title"] descv : movie[@"Year"] fv:[controller getFavoriteLocal:movie[@"imdbID"]] url:movie[@"Poster"] id:movie[@"imdbID"]]];
         }
         
         completion(listMoviesMutable);
         
         NSString * datares = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
         
         NSLog(@"Data %@",datares);
     }];
    
    [dataTask resume];
    
}

@end
