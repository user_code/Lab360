//
//  ApiMovies.h
//  MoviesLab
//
//  Created by maicon douglas on 18/08/20.
//  Copyright © 2020 maicon douglas. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "../Controllers/ViewController.h"
#import "../Models/Movie.h"

NS_ASSUME_NONNULL_BEGIN

#define urlapi "http://www.omdbapi.com/?s=marvel&apikey=d8db030e"

@interface ApiMovies : NSObject
- (void) getMovies : (ViewController *) controller :(void(^)(NSMutableArray<Movie *> *))handler;
@end

NS_ASSUME_NONNULL_END
